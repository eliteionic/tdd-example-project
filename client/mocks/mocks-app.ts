export const modulesServiceSpy = {
  getModules: jest.fn().mockReturnValue([{}]),
  getModuleById: jest.fn().mockReturnValue({}),
  getLessonById: jest.fn().mockReturnValue({}),
};

export const authServiceSpy = {
  checkKey: jest.fn(),
  reauthenticate: jest.fn(),
  logout: jest.fn(),
};
