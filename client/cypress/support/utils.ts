export const getModuleListItems = () => {
  return cy.get('.module-list ion-item');
};

export const getLessonListItems = () => {
  return cy.get('.lesson-list ion-item');
};

export const getLessonContent = () => {
  return cy.get('.lesson-content');
};

export const getKeyInput = () => {
  return cy.get('.key-input input');
};

export const getLoginButton = () => {
  return cy.get('.login-button');
};

export const getLogoutButton = () => {
  return cy.get('.logout-button');
};
