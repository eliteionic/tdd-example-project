import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { NavController } from '@ionic/angular';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root',
})
export class AuthService {
  public storage: Storage;

  constructor(private http: HttpClient, private navCtrl: NavController) {
    this.init();
  }

  init(): void {
    this.storage = localStorage;
  }

  checkKey(key: string): Observable<any> {
    this.storage.setItem('eliteLicenseKey', key);

    const body = {
      key: key,
    };

    return this.http.post('http://localhost:8080/api/check', body);
  }

  async reauthenticate(): Promise<void> {
    const key = this.storage.getItem('eliteLicenseKey');

    if (!key) {
      throw new Error('No key found');
    }

    this.checkKey(key).subscribe((res) => {
      if (res) {
        return true;
      } else {
        throw new Error('Invalid key');
      }
    });
  }

  async logout(): Promise<boolean> {
    this.storage.removeItem('eliteLicenseKey');
    return this.navCtrl.navigateRoot('/login');
  }
}
