export const navControllerSpy = {
  navigateForward: jest.fn(),
  navigateBackward: jest.fn(),
  navigateRoot: jest.fn(),
};

const loadingComponentSpy = {
  present: jest.fn().mockResolvedValue(true),
  dismiss: jest.fn().mockResolvedValue(true),
};

export const loadingControllerSpy = {
  create: jest.fn().mockResolvedValue(loadingComponentSpy),
};
