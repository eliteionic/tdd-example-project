import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { NavController } from '@ionic/angular';
import { ModulesService } from '../services/modules.service';
import { Module } from '../interfaces/module';
import { Lesson } from '../interfaces/lesson';

@Component({
  selector: 'app-lesson-select',
  templateUrl: './lesson-select.page.html',
  styleUrls: ['./lesson-select.page.scss'],
})
export class LessonSelectPage implements OnInit {
  public module: Module = {
    id: 0,
    title: '',
    description: '',
    lessons: [],
  };

  constructor(
    private navCtrl: NavController,
    private route: ActivatedRoute,
    private modulesService: ModulesService
  ) {}

  ngOnInit() {
    const id = this.route.snapshot.paramMap.get('id');
    this.module = this.modulesService.getModuleById(parseInt(id));
  }

  openLesson(lesson: Lesson) {
    this.navCtrl.navigateForward(
      '/module/' + this.module.id + '/lesson/' + lesson.id
    );
  }
}
