import { TestBed } from '@angular/core/testing';

import { ModulesService } from './modules.service';

describe('ModulesService', () => {
  let service: ModulesService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(ModulesService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('the getModules function should return an array', () => {
    expect(service.getModules() instanceof Array).toBe(true);
  });

  it('the getModuleById function should return a module when given a module id', () => {
    const testId = 1;
    const testModule = service.getModuleById(testId);

    expect(testModule.title).toBe('Module One');
  });

  it('the getLessonById function should return a lesson when given a module id and lesson id', () => {
    const testModuleId = 1;
    const testLessonId = 2;

    const testLesson = service.getLessonById(testModuleId, testLessonId);
    expect(testLesson.title).toBe('lesson2');
  });
});
