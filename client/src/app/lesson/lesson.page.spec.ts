import { ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';
import { ActivatedRoute } from '@angular/router';

import { ModulesService } from '../services/modules.service';
import { modulesServiceSpy } from '../../../mocks/mocks-app';

import { LessonPage } from './lesson.page';

describe('LessonPage', () => {
  let component: LessonPage;
  let fixture: ComponentFixture<LessonPage>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [LessonPage],
      providers: [
        {
          provide: ActivatedRoute,
          useValue: {
            snapshot: {
              paramMap: {
                get: (path) => {
                  return '1';
                },
              },
            },
          },
        },
        { provide: ModulesService, useValue: modulesServiceSpy },
      ],
      imports: [IonicModule.forRoot()],
    }).compileComponents();

    fixture = TestBed.createComponent(LessonPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should have a lesson class member', () => {
    expect(component.lesson).toBeDefined();
  });

  it('should set up the passed in lesson as the lesson class member', () => {
    const modulesService: any = fixture.debugElement.injector.get(
      ModulesService
    );

    const spy = jest.spyOn(modulesService, 'getLessonById').mockReturnValue({
      id: 1,
      title: 'lesson 1',
      content: 'this is the test content',
    });

    component.ngOnInit();

    expect(component.lesson.title).toBe('lesson 1');
  });
});
