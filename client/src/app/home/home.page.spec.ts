import { ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule, NavController } from '@ionic/angular';
import { navControllerSpy } from '../../../mocks/mocks-ionic';
import { authServiceSpy } from '../../../mocks/mocks-app';
import { modulesServiceSpy } from '../../../mocks/mocks-app';

import { HomePage } from './home.page';
import { AuthService } from '../services/auth.service';
import { ModulesService } from '../services/modules.service';

describe('HomePage', () => {
  let component: HomePage;
  let fixture: ComponentFixture<HomePage>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [HomePage],
      providers: [
        {
          provide: AuthService,
          useValue: authServiceSpy,
        },
        {
          provide: NavController,
          useValue: navControllerSpy,
        },
        {
          provide: ModulesService,
          useValue: modulesServiceSpy,
        },
      ],
      imports: [IonicModule.forRoot()],
    }).compileComponents();

    fixture = TestBed.createComponent(HomePage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should have a class member called modules that is an array', () => {
    expect(component.modules instanceof Array).toBe(true);
  });

  it('the modules class member should contain 5 modules after ngOnInit has been triggered', () => {
    const moduleService = fixture.debugElement.injector.get(ModulesService);

    const dummyModule = {
      id: 0,
      title: '',
      description: '',
      lessons: [],
    };

    jest
      .spyOn(moduleService, 'getModules')
      .mockReturnValue(new Array(5).fill(dummyModule));

    component.ngOnInit();

    expect(component.modules.length).toBe(5);
  });

  it('the openModule() function should navigate to the LessonListPage', () => {
    const navCtrl = fixture.debugElement.injector.get(NavController);
    const testModule = { title: 'pretend module', id: 1 };

    component.openModule(testModule.id);

    expect(navCtrl.navigateForward).toHaveBeenCalledWith(
      '/module/' + testModule.id
    );
  });

  it('the logout function should call the logout method of the auth provider', () => {
    const authService = fixture.debugElement.injector.get(AuthService);

    jest.spyOn(authService, 'logout');

    component.logout();

    expect(authService.logout).toHaveBeenCalled();
  });
});
